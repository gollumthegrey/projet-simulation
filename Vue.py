# -*- coding: utf-8 -*-
from tkinter import *
from PIL import Image, ImageTk
import random

class Vue():
    def __init__(self,parent,largeur,hauteur):
        self.largeur=largeur
        self.hauteur=hauteur
        self.parent=parent
        self.root=Tk()
        self.root.title("Simulation")
        self.creercadreenviro()
        self.target=None
        
 #*************ESSAI************************************
    def callback(self,event):

        self.target=None
        #TEMPORARAIRE
        E = self.parent.modele.environnement

        foo = event.widget.find_withtag("current")

        foo = self.canevas.gettags(foo)
        if foo[2] != 'current':
            foo = int(foo[2])
        
        for i in E.loups:
            if i.id == foo:
                self.target=i
        for i in E.lapins:
            if i.id == foo:
                self.target=i
        for i in E.laitues:
            if i.id == foo:
                self.target=i
        for i in E.carcasses:
            if i.id == foo:
                self.target=i
                
       
        
    #==================== CREER CADRE PRINCIPAL ====================
    def creercadreenviro(self):
        #-------------- Cadre Principal --------------
        
        self.cadrecanevas=Frame(self.root)
        self.canevas=Canvas(self.cadrecanevas, width=self.largeur, height=self.hauteur,bg="tan")        
        self.canevas.tag_bind("chose",'<Button-1>', self.callback)

        #-------------- Cadre Outils --------------
        self.cadreoutils=Frame(self.root)
        self.nbrloup=Entry(self.cadreoutils)
        self.nbrloup.insert(0,"5")
        self.nbrlapin=Entry(self.cadreoutils)
        self.nbrlapin.insert(0,"20")
        self.nbrlaitue=Entry(self.cadreoutils)
        self.nbrlaitue.insert(0,"40")
        self.vitesseSim=Entry(self.cadreoutils)
        self.vitesseSim.insert(0,"1")
        
        self.entryLarg=Entry(self.cadreoutils)
        self.entryLarg.insert(0, "800")
        self.entryHaut=Entry(self.cadreoutils)
        self.entryHaut.insert(0, "500")
        self.vitesseSim=Label(self.cadreoutils, width=5, text="Normal")
        
        self.vitMinus=Button(self.cadreoutils, command=self.vitLess, text="-")
        self.vitPlus=Button(self.cadreoutils, command=self.vitMore, text="+")
        
        self.hbar=Scrollbar(self.cadrecanevas, command=self.canevas.xview, orient=HORIZONTAL)
        self.vbar=Scrollbar(self.cadrecanevas, command=self.canevas.yview, orient=VERTICAL)
        
        #-------------- Label Textes --------------
        nbrloup=Label(self.cadreoutils,text="Loups")
        nbrlapin=Label(self.cadreoutils,text="Lapins")
        nbrlaitue=Label(self.cadreoutils,text="Laitues")
        largEnviro=Label(self.cadreoutils,text="Largeur")
        hautEnviro=Label(self.cadreoutils,text="Hauteur")
        vitesseSim=Label(self.cadreoutils,text="Vitesse")
        
        self.qtelaitues=Label(self.cadreoutils)
        self.qtelapins=Label(self.cadreoutils)
        self.qteloups=Label(self.cadreoutils)
        
        #-------------- Grid les Widgets --------------
        
        vitesseSim.grid(column=0,row=0)
        self.vitMinus.grid(column=1,row=0)
        self.vitesseSim.grid(column=2,row=0)
        self.vitPlus.grid(column=5,row=0)
        
        largEnviro.grid(column=6,row=0)
        self.entryLarg.grid(column=7,row=0)
        hautEnviro.grid(column=8,row=0)
        self.entryHaut.grid(column=9,row=0)
        
        nbrloup.grid(column=0,row=1)
        self.nbrloup.grid(column=2,row=1)
        nbrlapin.grid(column=6,row=1)
        self.nbrlapin.grid(column=7,row=1)
        nbrlaitue.grid(column=8,row=1)
        self.nbrlaitue.grid(column=9,row=1)
        
        
        self.cadreoutils.pack(side=BOTTOM)
        self.cadrecanevas.pack(side=LEFT)
        
        #-------------- Importer Images --------------
        
        self.photoLapin = []
        for i in range(9):
            for j in range(3):
                if i+1 != 5:
                    var = "./Images/Lapins/lapin" + str(i+1) + "_" + str(j+1) + ".png"
                    image = Image.open(var)
                    self.photoLapin.append(ImageTk.PhotoImage(image))
        

        self.imageForet = Image.open("./Images/foret.png")
        self.imageForet = self.imageForet.resize((60, 60), Image.ANTIALIAS)
        self.photoForet = ImageTk.PhotoImage(self.imageForet)   
        
        self.imageMontagne = Image.open("./Images/montagne.png")
        self.imageMontagne = self.imageMontagne.resize((100,100), Image.ANTIALIAS)
        self.photoMontagne = ImageTk.PhotoImage(self.imageMontagne) 
        
        image = Image.open("./Images/TastyMeatSprite.png")
        image = image.resize((15,12), Image.ANTIALIAS)
        self.photoViande = ImageTk.PhotoImage(image)
        
        self.imageLoup = Image.open("./Images/wolf.png")
        self.photoLoup = ImageTk.PhotoImage(self.imageLoup)
        self.imageLaitue = Image.open("./Images/lettuce.png")
        self.photoLaitue = ImageTk.PhotoImage(self.imageLaitue)
        self.imageLoupD = Image.open("./Images/wolfDroite.png")
        self.photoLoupD = ImageTk.PhotoImage(self.imageLoupD)
        
        
        #-------------- Button GO --------------
        btngo=Button(self.cadreoutils,text="CRÉER",command=self.creerenviro, bg="lightgreen", font="Arial", width=10)
        self.btnpa=Button(self.cadreoutils,text="PAUSE",command=self.pause, bg="pink", font="Arial", width=10)
        btngo.grid(column=10,row=0)
        self.btnpa.grid(column=10,row=1)
        
    def vitMore(self):
        dict = {"Lent":60, "Normal":30, "Rapide":1}
        var = self.vitesseSim.cget("text")
        if var == "Lent":
            var = "Normal"
            self.parent.changerVitesse(dict[var])
            self.vitesseSim.config(text=var)
        elif var == "Normal":
            var = "Rapide"
            self.parent.changerVitesse(dict[var])
            self.vitesseSim.config(text=var)
            
    def vitLess(self):
        dict = {"Lent":60, "Normal":30, "Rapide":1}
        var = self.vitesseSim.cget("text")
        if var == "Normal":
            var = "Lent"
            self.parent.changerVitesse(dict[var])
            self.vitesseSim.config(text=var)
        elif var == "Rapide":
            var = "Normal"
            self.parent.changerVitesse(dict[var])
            self.vitesseSim.config(text=var)
        
        
    def resume(self):
        
        #temp
        self.parent.go=True
        #temp
        
        self.btnpa.config(text="PAUSE", command=self.pause, bg="pink")
        
        self.nbrloup.grid_forget()
        self.nbrlapin.grid_forget()
        self.nbrlaitue.grid_forget()
        
        self.qteloups.grid(column=2, row=1)
        self.qtelapins.grid(column=7, row=1)
        self.qtelaitues.grid(column=9, row=1)
        
        
    def pause(self):
        
        #temp
        Loups = self.parent.modele.environnement.loups
        Lapins = self.parent.modele.environnement.lapins
        Laitues = self.parent.modele.environnement.laitues
        self.parent.go=False
        #temp
        
        self.btnpa.config(text="RÉSUMER", command=self.resume, bg="lightblue")
        
        self.qteloups.grid_forget()
        self.qtelapins.grid_forget()
        self.qtelaitues.grid_forget()
        
        self.nbrloup.grid(column = 2, row=1)
        self.nbrlapin.grid(column = 7, row=1)
        self.nbrlaitue.grid(column = 9, row=1)
        
        self.nbrloup.delete(0, END)
        self.nbrlapin.delete(0, END)
        self.nbrlaitue.delete(0, END)
        
        self.nbrloup.insert(0, len(Loups))
        self.nbrlapin.insert(0, len(Lapins))
        self.nbrlaitue.insert(0, len(Laitues))
        
    
    
    
    #==================== DÉBUTER SIMULATION ====================
    
    def creerenviro(self):
        self.target=None
        
        
        self.nbrloup.grid_forget()
        self.nbrlapin.grid_forget()
        self.nbrlaitue.grid_forget()
        self.qteloups.grid(column = 1, row=1)
        self.qtelapins.grid(column = 3, row=1)
        self.qtelaitues.grid(column = 5, row=1)
        
        
        self.largeur=round(float(self.entryLarg.get()))
        self.hauteur=round(float(self.entryHaut.get()))
        self.canevas.config(width=self.largeur, height=self.hauteur)
        nloup=round(float(self.nbrloup.get()))
        nlapin=round(float(self.nbrlapin.get()))
        nlaitue=round(float(self.nbrlaitue.get()))
                      
        self.parent.creerenviro(nloup,nlapin,nlaitue)
        
        self.canevas.config(xscrollcommand=self.hbar.set, yscrollcommand=self.vbar.set, scrollregion=(0,0,self.largeur,self.hauteur))
        self.vbar.pack(side=RIGHT, fill=Y)
        self.hbar.pack(side=BOTTOM, fill=X)
        self.canevas.pack(side=LEFT, expand=True, fill=BOTH)
        self.resume()
   
    
    
    
    #==================== DESSINER RIVIERE ====================
    def dessinerRiviere(self, enviro):
        self.canevas.delete("biome")
        i = enviro.coordRiviere
        
        self.canevas.create_line(i[0][0], i[0][1], 
                                 i[1][0], i[1][1], 
                                 i[2][0], i[2][1], 
                                 i[3][0], i[3][1], 
                                 fill="lightblue", width=40, smooth=True, tags=("riviere","biome"))    
        j = enviro.coordForet
        self.canevas.create_image(j[0], j[1], image=self.photoForet, tags=("foret","biome"))

        j = enviro.coordMontagne
        self.canevas.create_image(j[0], j[1], image=self.photoMontagne, tags=("montagne","biome")) 

    
               
        
    #==================== DESSINER OBJETS ====================
    def afficheenviro(self,enviro):
        self.canevas.delete("chose", "target")
        dict={'1': [self.photoLapin[0], self.photoLapin[1], self.photoLapin[2]],    '2': [self.photoLapin[3], self.photoLapin[4], self.photoLapin[5]],
              '3': [self.photoLapin[6], self.photoLapin[7], self.photoLapin[8]],    '4': [self.photoLapin[9], self.photoLapin[10], self.photoLapin[11]],
              '6': [self.photoLapin[12], self.photoLapin[13], self.photoLapin[14]], '7': [self.photoLapin[15], self.photoLapin[16], self.photoLapin[17]],
              '8': [self.photoLapin[18], self.photoLapin[19], self.photoLapin[20]], '9': [self.photoLapin[21], self.photoLapin[22], self.photoLapin[23]]}
        
        for i in enviro.carcasses:
            if i.type == 'R':
                self.canevas.create_image(i.x,i.y, image=self.photoViande,tags=("chose","carcasse",i.id))
            else:
                self.canevas.create_image(i.x,i.y, image=self.photoLaitue,tags=("chose","carcasse",i.id))
            
            
        for i in enviro.laitues:
            self.canevas.create_image(i.x,i.y, image=self.photoLaitue,tags=("chose","laitue",i.id))
        
        
        for i in enviro.lapins:
            self.canevas.create_image(i.x, i.y, image=dict[i.direction][i.animation], tags=("chose","lapin",i.id))
                
                                    
        for i in enviro.loups:
            if i.direction == '4':
                self.canevas.create_image(i.x,i.y, image=self.photoLoup,tags=("chose","loup",i.id))
            else:
                self.canevas.create_image(i.x,i.y, image=self.photoLoupD,tags=("chose","loup",i.id))
        
            
        if self.target:
            i = self.target
            self.canevas.create_rectangle(i.x-20, i.y-20, i.x+20, i.y+20, tags=("target"))
            var = "id: " + str(i.id)
            var2 = "energie: " + str(int(i.energie))
            var3 = "sexe: " + str(i.sexe)
            if i.x > self.largeur-100:
                self.canevas.create_text(i.x-25, i.y-15, text=var, anchor=E, tags=("target"))
                self.canevas.create_text(i.x-25, i.y, text=var2, anchor=E, tags=("target"))
                if i.sexe:
                    self.canevas.create_text(i.x-25, i.y+15, text=var3, anchor=E, tags=("target"))
            else:
                self.canevas.create_text(i.x+25, i.y-15, text=var, anchor=W, tags=("target"))
                self.canevas.create_text(i.x+25, i.y, text=var2, anchor=W, tags=("target"))
                if i.sexe:
                    self.canevas.create_text(i.x+25, i.y+15, text=var3, anchor=W, tags=("target"))
         
            
    #==================== ALTERNER JOUR / NUIT ====================        
    def jourNuit(self, jour):
        if (not jour):
            self.canevas.config(bg="AntiqueWhite4")
            self.canevas.itemconfig("riviere", fill="deepskyblue4")
        else:
            self.canevas.config(bg="tan")
            self.canevas.itemconfig("riviere", fill="lightblue")
    
    #=================== AFFICHAGE DES QUANTITï¿½S =================        
    def afficheQtes(self, listeLaitue, listeLapin, listeLoup):
        self.qtelaitues.config(text=len(listeLaitue))
        self.qtelapins.config(text=len(listeLapin))
        self.qteloups.config(text=len(listeLoup))
        
   
        