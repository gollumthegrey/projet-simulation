# -*- coding: utf-8 -*-
from helper import Helper
from Carcasse import *
import random, math, Laitue

class Animal(object):
    def __init__(self,x,y,energie,poids,sexe,id):
        self.x=x
        self.y=y
        self.id=id
        self.energie=energie
        self.poids=poids
        self.sexe=sexe
        self.age=0
        self.ciblebouffer=None
        self.repas=None
        self.besoinreproduction=False
        self.cible=False
        self.cibleXY=[None,None]
        self.direction = '4'
        self.ciblereproduction=None
        
        self.fatigue=0
            
            
     
    # ----------------- IDLE ------------------
    # =========================================              
    def idle(self, parent):
        dist = 20
        vit = self.vitesse*0.15
        if not self.cible:
            self.cible=True
            self.cibleXY[0] = random.randint(round(self.x)-dist, round(self.x)+dist) 
            self.cibleXY[1] = random.randint(round(self.y)-dist, round(self.y)+dist)
            
            if (self.x < 20):
                self.cibleXY[0] = self.x+dist
            elif (self.x > parent.largeur-20):
                self.cibleXY[0] = self.x-dist
                
            if (self.y < 20):
                self.cibleXY[1] = self.y+dist
            elif(self.y > parent.hauteur-20):
                self.cibleXY[1] = self.y-dist
                
            if (self.x<0 or self.x>parent.largeur or self.y<0 or self.y>parent.hauteur):
                vit = self.vitesse
                  
            self.getDirection(self.cibleXY[0], self.cibleXY[1])
                
        if self.cible:
            if(Helper.calcDistance(self.x,self.y, self.cibleXY[0], self.cibleXY[1])< vit ):
                angle=Helper.calcAngle(self.x, self.y, self.cibleXY[0], self.cibleXY[1])
                dist2=Helper.calcDistance(self.x, self.y, self.cibleXY[0], self.cibleXY[1])
                nouvelleposition = Helper.getAngledPoint(angle, dist2, self.x, self.y)
                self.cible=None
                self.cibleXY = [None, None]
                self.x = round(self.x)
                self.y = round(self.y)
                
            else:
                angle=Helper.calcAngle(self.x, self.y, self.cibleXY[0], self.cibleXY[1])
                nouvelleposition = Helper.getAngledPoint(angle, vit, self.x, self.y)
                self.x = nouvelleposition[0]
                self.y = nouvelleposition[1]
                self.getDirection(self.cibleXY[0], self.cibleXY[1])
                
    
    # -------- DIRECTION (animation) ----------
    # =========================================            
    def getDirection(self, X, Y):
        
              
        Angle=Helper.calcDegre(self.x, self.y, X, Y)
        
              
        if Angle<=30 and Angle>=0 or Angle<=360 and Angle>=330  :
            self.direction = '6'
     
        elif Angle<60 and Angle>30:
            self.direction = '9'

        elif Angle<120 and Angle>60:
            self.direction = '8'

        elif Angle<150 and Angle>120:
            self.direction = '7'
        
        elif Angle<=210 and Angle>=150:
            self.direction = '4'
        
        elif Angle<240 and Angle>210:
            self.direction = '1'
       
        elif Angle<300 and Angle>240:
            self.direction = '2'      
        
        elif Angle<330 and Angle>300:
            self.direction = '3'   
        
    
    # ----------------- MANGER ----------------
    # =========================================
    def manger(self,parent):
        if(self.energie<100 and self.repas):
            self.repas.manger(0.2)
            self.energie+=0.2
        else:
            self.repas=None
            
     
            
    # ---------------- CHASSER ----------------
    # =========================================
    def chasser(self, parent, objectSELF, objectTARGET):
        if self.ciblebouffer:
            #-------------- Aller sur la Cible --------------
            if(Helper.calcDistance(self.x,self.y,self.ciblebouffer.x,self.ciblebouffer.y)<self.vitesse):
                nouvelleposition=Helper.getAngledPoint(Helper.calcAngle(self.x,self.y,self.ciblebouffer.x,self.ciblebouffer.y),Helper.calcDistance(self.x,self.y,self.ciblebouffer.x,self.ciblebouffer.y),self.x,self.y)
                self.x=nouvelleposition[0]
                self.y=nouvelleposition[1]
            else:
                nouvelleposition=Helper.getAngledPoint(Helper.calcAngle(self.x,self.y,self.ciblebouffer.x,self.ciblebouffer.y),self.vitesse,self.x,self.y)
                self.x=nouvelleposition[0]
                self.y=nouvelleposition[1]
                self.getDirection(self.ciblebouffer.x, self.ciblebouffer.y)
                
            #-------------- Tuer la Cible --------------
            if(self.x==self.ciblebouffer.x and self.y==self.ciblebouffer.y):
                cibleManger=self.ciblebouffer
                var = 'R'
                if cibleManger.sexe == None:
                    var = 'V'
                parent.carcasses.append(Carcasse(cibleManger.x, cibleManger.y, self.ciblebouffer.energie, parent.id, var))
                parent.id += 1
                self.repas = parent.carcasses[-1]
                
                if objectTARGET.count(self.ciblebouffer):
                    objectTARGET.remove(self.ciblebouffer)
                    for i in objectSELF:
                        if(i.ciblebouffer==cibleManger):
                            i.ciblebouffer=None
                self.fatigue=0
                        
        else:
            
            #-------------- Acquérir une Cible --------------
            cibletemp=[None,0]
            for i in objectTARGET:
                dist=Helper.calcDistance(self.x,self.y,i.x,i.y)
                if cibletemp[0]:
                    if(dist<cibletemp[1]):
                        cibletemp=[i,dist]
                else:
                    cibletemp=[i,dist]
            self.ciblebouffer=cibletemp[0]
            
            