# -*- coding: utf-8 -*-
from helper import Helper
from Carcasse import *
import random,math
from Animal import *

class Lapin(Animal):
    def __init__(self,x,y,energie,poids,sexe,id):
        super(Lapin, self).__init__(x,y,energie,poids,sexe,id)
        
        self.champvision=30
        self.vitesse=2.0
        self.predateur=None
        self.animation=0
        self.enceinte=[False,0]
        
        
    #============= COMPORTEMENTS DU LAPIN =============#
    def evolue(self,parent,laitues,lapins):
        self.age+=1
        
        if self.age%9==0:
            self.animation += 1
            if self.animation >= 3:
                self.animation = 0
                
        if self.enceinte[0]:
            self.enceinte[1]+=1
            self.energie-=0.02
            if self.enceinte[1]==3500:
                parent.reproductionLapins(self.x,self.y)
                self.enceinte[0]=False
                self.enceinte[1]=0        
                
        
        if self.age%70==0:
            self.energie-=1
            if self.age>5000 and not self.enceinte[0]:
                self.besoinreproduction=True
            
        if self.energie <= 0:
            parent.carcasses.append(Carcasse(self.x, self.y, 10, parent.id, 'R'))
            parent.id += 1
            parent.lapins.remove(self)
        
        
        self.sursesgarde(parent,parent.loups)
        if(self.predateur):
            self.fuire(parent)        
        elif self.age>5000 and self.energie>60 and self.besoinreproduction==True:
            self.reproduction(parent,lapins)
       
        elif(self.repas):
            self.manger(parent)
        
        elif self.energie<50:
            if laitues:
                self.chasser(parent,lapins,laitues) 
            else:
                self.idle(parent)
            
        else:
            self.idle(parent)
            
    
    #============= REPRODUCTION ENTRE LAPINS =============#
    def reproduction(self,parent,lapins):
        if self.ciblereproduction:
            if(Helper.calcDistance(self.x,self.y,self.ciblereproduction.x,self.ciblereproduction.y)<self.vitesse):
                nouvelleposition=Helper.getAngledPoint(Helper.calcAngle(self.x,self.y,self.ciblereproduction.x,self.ciblereproduction.y),Helper.calcDistance(self.x,self.y,self.ciblereproduction.x,self.ciblereproduction.y),self.x,self.y)
                self.x=nouvelleposition[0]
                self.y=nouvelleposition[1]
            else:
                nouvelleposition=Helper.getAngledPoint(Helper.calcAngle(self.x,self.y,self.ciblereproduction.x,self.ciblereproduction.y),self.vitesse,self.x,self.y)
                self.x=nouvelleposition[0]
                self.y=nouvelleposition[1]
                self.getDirection(self.ciblereproduction.x,self.ciblereproduction.y)

            if(self.x==self.ciblereproduction.x and self.y==self.ciblereproduction.y):
                #CHANGEMENT
                if(self.sexe=="F"):
                    self.enceinte[0]=True
                else:
                    self.ciblereproduction.enceinte[0]=True
                
                cible=self.ciblereproduction
                self.besoinreproduction=False
                self.ciblereproduction.besoinreproduction=False
                for i in parent.lapins:
                        if(i.ciblereproduction==cible):
                            i.ciblereproduction=None    
                        
        else:
            cibletemp=[None,0]
            for i in lapins:
                if(i.sexe!=self.sexe and i.besoinreproduction):
                    dist=Helper.calcDistance(self.x,self.y,i.x,i.y)
                    if cibletemp[0]:
                        if(dist<cibletemp[1]):
                            cibletemp=[i,dist]
                    else:
                        cibletemp=[i,dist]
            self.ciblereproduction=cibletemp[0] 
            
            
            
    def sursesgarde(self,parent,loups):
        #-------- Champs de vision ----------
        if(self.repas):
            vision=self.champvision*0.5
        elif(self.ciblebouffer):
            vision=self.champvision*0.4
        else:
            vision=self.champvision
            
        #-------- Vérifier les alentours ----------
        for i in loups:
            if(Helper.calcDistance(i.x,i.y,self.x,self.y)<=vision):
                    self.predateur=i
                    
        #-------- Le loup Trouvé me chasse? --------          
        if(self.predateur):
            self.ciblebouffer=None
            self.repas=None
            self.ciblereproduction=None
            self.cible=None
            self.cibleXY=[None, None]
            
            
    def fuire(self, parent):
        vitessefuite=self.vitesse*(2*(self.energie/100))
        anglefuite=Helper.calcAngle(self.predateur.x,self.predateur.y,self.x,self.y)
        anglefuite+=random.uniform(-(math.pi/8),(math.pi/8))
        postemp=Helper.getAngledPoint(anglefuite,vitessefuite,self.x,self.y)
        self.getDirection(postemp[0],postemp[1])
        if postemp[0] > 0 and postemp[0] < parent.largeur:
            self.x=postemp[0]
        if postemp[1] > 0 and postemp[1] < parent.hauteur:
            self.y=postemp[1]
        
        self.energie-=0.1
        if(Helper.calcDistance(self.predateur.x,self.predateur.y,self.x,self.y)>150):
            self.predateur=None