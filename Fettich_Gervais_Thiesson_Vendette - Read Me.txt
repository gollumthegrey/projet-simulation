Loïc Fettich, Philippe Gervais, Gaëtan Thiesson et Robert Jr. Vendette

Read Me Simulateur Loups-Lapins-Laitues

Version 3.0

PARAMÈTRES ET INITIALISATION: 
============================================ 

Le simulateur permet de choisir les éléments que nous voulons voir évoluer dans
l'aire de simulation.

Pour l'instant, il est possible de choisir le nombre de
loups, de lapins et de laitues que l'utilisateur veut voir sur la grille. 

Il y a un champ «Vitesse simulation» qui permet de choisir la vitesse à laquelle la
simulation se déroule. 1 étant la vitesse la plus rapide, il est  ensuite
possible de ralentir la vitesse afin d'obtenir un plus grand réalisme.

Les dimensions de la carte de jeu sont paramétrables, ce qui rend la grille
ajustable à tout type d'écran.
Pour le moment, aucune interaction n’est possible avec la grille de simulation.

NOTES SUR LE COMPORTEMENT DES ANIMAUX :
============================================ 

1. Les lapins ont une énergie qui est générée de façon aléatoire et 
diminue avec les cycle d'évolution de la simulation. Quand leur niveau
d'énergie diminue en-deçà du seuil prédéfini, les lapins se mettent 
à la recherche d’une cible à manger. Ils vont alors trouver la laitue 
la plus près et la manger.
Quand ils mangent les laitues, ils obtiennent de l'énergie des laitues 
(qui est aussi générée aléatoirement et qui diminue également avec le
passage du temps).
Quand les loups débutent leur chasse, les lapins le détectent et prennent
la fuite, mais ne peuvent pas sortir en dehors du cadre de la grille.

2. Les loups ont eux aussi une énergie qui est générée de façon aléatoire 
et diminue également au fur et à mesure que la simulation se déroule. 
Quand leur niveau d’énergie est suffisamment bas, ils se mettent en chasse 
et courent pour attraper un lapin. Si le loup ne parvient pas à attraper un 
lapin, il perdra toute son énergie et finira par mourir et devenir une carcasse 
(un «X» rouge). Pour le moment, les loups ne peuvent pas se reproduire. 

3. Les laitues vont être générée au début de la simulation et vont elles 
aussi perdre de l’énergie (qui est aussi générée aléatoirement) tout au 
long de leur vie. Quand leur énergie atteint 0, elles disparaissent de la 
grille et vont réapparaître de façon aléatoire sur la grille.



