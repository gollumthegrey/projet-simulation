# -*- coding: utf-8 -*-
import Environnement
import random

class Monde():
    def __init__(self, parent, largeur,hauteur):
        self.parent = parent
        self.largeur=largeur
        self.hauteur=hauteur
        self.compteur=0
        self.jour = True
        self.journee=1
        self.saison=0
        self.environnement=None
        self.coordRiviere = [[None,None], [None,None], [None,None], [None,None]]
        self.genererRiviere()
		
        self.coordForet=[None,None]
        self.coordMontagne=[None, None]
        
        self.genererForet()
        self.genererMontagne()
        
        
	#==================== GÉNÉRER NOUVEL ENVIRONNEMENT ====================	
    def creerenviro(self,loup,lapin,laitue):
        self.environnement=Environnement.Environnement(self.largeur,self.hauteur,int(loup),int(lapin),int(laitue), self.coordRiviere, self.coordForet,self.coordMontagne)
        
		
	#==================== UPDATE LE MONDE ====================
    def evolue(self):
        if self.environnement:
            self.changerJournee()   
            self.environnement.evolue()
            
			
	#==================== COMPTEUR ====================
    def changerJournee(self):
        self.compteur += 1
		
        if (self.compteur % 2500 == 0):
            #--------------Safe Salade--------------------------
            if(len(self.environnement.laitues)<20):
                for i in range(random.randrange(10,40)):
                    x=random.randrange(self.largeur)
                    y=random.randrange(self.hauteur)
                    self.environnement.reproductionLaitue(x,y)
                    
            #--------------Safe Lapin--------------------------
            if(len(self.environnement.lapins)<5):
                for i in range(random.randrange(1,5)):
                    x=random.randrange(self.largeur)
                    y=random.randrange(self.hauteur)
                    self.environnement.reproductionLapins(x,y)
        
		#-------------- Cycle Jour / Nuit --------------
        if (self.compteur % 5000 == 0):
            if (self.jour):
                self.jour=False
                self.parent.jourNuit(self.jour)
            else:
                self.jour=True
                self.parent.jourNuit(self.jour)
                self.journee += 1
                
                
		#-------------- Changer de saison --------------
        if (self.journee % 90):
            if (self.saison == 3):
                self.saison=0
            else:
                self.saison+=1
   
    #==================== GÉNÉRER RIVIERE ====================                
    def genererRiviere(self):
        temp = None
        #---- random debut de riviere (commence du côté ou en haut, et quelle direction)
        #DEB = random.randint(1,2)
        #DIR = random.randint(1,2)
        
        X = round(self.largeur/3)
        Y = round(self.hauteur/3)
        
        #if (DEB is 1 and DIR is 1):
        lastCoordX= -55 - X
        lastCoordY= 0 - Y
        for i in self.coordRiviere:
            i[0] = lastCoordX + X + self.fRand()
            i[1] = lastCoordY + Y + self.fRand()
            lastCoordX = i[0]
            lastCoordY = i[1]
        self.coordRiviere[0][0] = -150
        self.coordRiviere[0][1] = -150
        self.coordRiviere[3][0] = self.largeur + 150
        self.coordRiviere[3][1] = self.hauteur + 150
                
    def fRand(self):
        var = random.randint(-250,250)
        return var
    
    
    def genererForet(self):
        x=random.randrange(self.largeur)
        y=random.randrange(self.hauteur)
        self.coordForet=[x,y]
        
    def genererMontagne(self):
        x=random.randrange(self.largeur)
        y=random.randrange(self.hauteur)
        self.coordMontagne=[x,y]