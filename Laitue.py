import random
import Carcasse  
        
class Laitue():
    def __init__(self,x,y,energie,id):
        self.x=x
        self.y=y
        self.id=id
        self.energie=energie
        self.age=0
        self.sexe=None

    def evolue(self, parent):
        self.age +=1
        
        if self.age%70 == 0:
            self.energie -= 1
        
        if self.energie < 0:
            self.reproduction(parent)
            for i in parent.lapins:
                if(i.ciblebouffer==self):
                    i.ciblebouffer=None
            parent.laitues.remove(self)
            
        
    def reproduction(self, parent):
        parent.reproductionLaitue(self.x,self.y)