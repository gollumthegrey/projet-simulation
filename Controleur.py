# -*- coding: utf-8 -*-
import Vue
import Monde
        
class Controleur():
    def __init__(self):
        self.vitesse=15
        self.go=True
        self.modele=None
        self.vue=Vue.Vue(self,largeur=800,hauteur=500)
        self.timercourant=None
        self.vue.root.mainloop()
        
    #-------------- CrÃƒÂ©er un nouvel Environnement --------------    
    def creerenviro(self,loup,lapin,laitue):
        if self.timercourant:
            self.resetSim()
        self.modele=Monde.Monde(self, largeur=self.vue.largeur,hauteur=self.vue.hauteur)
        self.modele.creerenviro(loup,lapin,laitue)
        self.vue.afficheenviro(self.modele.environnement)
        self.vue.dessinerRiviere(self.modele)
        self.montimer()
     
    #-------------- Changer la vitesse --------------   
    def changerVitesse(self, var):
        self.vitesse=var
        
	#-------------- Update le Timer --------------
    def montimer(self):
        if self.go:
            self.modele.evolue()
        self.vue.afficheQtes(self.modele.environnement.laitues, self.modele.environnement.lapins, 
        self.modele.environnement.loups)   
        self.vue.afficheenviro(self.modele.environnement)
        self.timercourant=self.vue.root.after(self.vitesse,self.montimer)
    
     
	#-------------- Afficher cycle Jour / Nuit --------------
    def jourNuit(self, jour):
        self.vue.jourNuit(jour)
     
	#-------------- Reset le Timer --------------
    def resetSim(self):
        self.vue.root.after_cancel(self.timercourant)
        self.vue.jourNuit(True)


if __name__ == '__main__':
    c=Controleur()