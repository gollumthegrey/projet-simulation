# -*- coding: utf-8 -*-
from helper import Helper
from Carcasse import *
from Animal import *
import random

class Loup(Animal):
    def __init__(self,x,y,energie,poids,sexe,id):
        super(Loup, self).__init__(x,y,energie,poids,sexe,id)
        
        self.vitesse=2.5
        self.forceIdle=0
        self.enceinte=[False,0]
        self.ciblecharognard=None

    #============= COMPORTEMENTS DU LOUP =============#
    # Copyright © Gäetan
    def evolue(self, parent, loups, lapins):
        
        if self.enceinte[0]:
            self.enceinte[1]+=1
            self.energie-=0.03
            if self.enceinte[1]==5000:
                parent.reproductionLoups(self.x,self.y)
                self.enceinte[0]=False
                self.enceinte[1]=0
        
        self.age+=1
        if self.age%80==0:
            self.energie-=1
            if self.age>10000 and not self.enceinte[0]:
                    self.besoinreproduction=True
            
        if self.energie<=0:
                parent.carcasses.append(Carcasse(self.x, self.y, 15, parent.id, 'R'))
                parent.id += 1
                parent.loups.remove(self)
            
        if self.energie > 100:
            self.energie =100
    
        if(self.forceIdle>0):
            self.forceIdle-=1
            self.idle(parent)
        
        if(self.cible):
            self.idle(parent)
        elif(self.repas):
            self.manger(parent)
        elif self.energie>60 and self.besoinreproduction==True:
            self.reproduction(parent)
        elif (self.energie<=55):
            self.charognard(parent)
            if not self.ciblecharognard:
                self.chasser(parent, loups, lapins)
            if self.ciblebouffer:
                self.abandonner()
        else:
            self.idle(parent)
            
            
    def abandonner(self):
        self.energie-=0.015
        self.fatigue+=5
        if(self.fatigue>=1250):
            self.ciblebouffer=None
            self.fatigue=0
            self.forceIdle+=150
            
    
    #=============Reproduction loup==============
    def reproduction(self,parent):
        if self.ciblereproduction:
            if(Helper.calcDistance(self.x,self.y,self.ciblereproduction.x,self.ciblereproduction.y)<self.vitesse):
                nouvelleposition=Helper.getAngledPoint(Helper.calcAngle(self.x,self.y,self.ciblereproduction.x,self.ciblereproduction.y),Helper.calcDistance(self.x,self.y,self.ciblereproduction.x,self.ciblereproduction.y),self.x,self.y)
                self.x=nouvelleposition[0]
                self.y=nouvelleposition[1]
            else:
                nouvelleposition=Helper.getAngledPoint(Helper.calcAngle(self.x,self.y,self.ciblereproduction.x,self.ciblereproduction.y),self.vitesse,self.x,self.y)
                self.x=nouvelleposition[0]
                self.y=nouvelleposition[1]
                self.getDirection(nouvelleposition[0],nouvelleposition[1])

            if(self.x==self.ciblereproduction.x and self.y==self.ciblereproduction.y):
                #CHANGEMENT
                if(self.sexe=="F"):
                    self.enceinte[0]=True
                else:
                    self.ciblereproduction.enceinte[0]=True
                
                cible=self.ciblereproduction
                self.besoinreproduction=False
                self.ciblereproduction.besoinreproduction=False
                for i in parent.loups:
                        if(i.ciblereproduction==cible):
                            i.ciblereproduction=None    
                        
        else:
            cibletemp=[None,0]
            for i in parent.loups:
                if(i.sexe!=self.sexe and i.besoinreproduction):
                    dist=Helper.calcDistance(self.x,self.y,i.x,i.y)
                    if cibletemp[0]:
                        if(dist<cibletemp[1]):
                            cibletemp=[i,dist]
                    else:
                        cibletemp=[i,dist]
            self.ciblereproduction=cibletemp[0]
    
    def charognard(self,parent):
        if self.ciblecharognard:
            #-------------- Aller sur la Cible --------------
            if(Helper.calcDistance(self.x,self.y,self.ciblecharognard.x,self.ciblecharognard.y)<self.vitesse):
                nouvelleposition=Helper.getAngledPoint(Helper.calcAngle(self.x,self.y,self.ciblecharognard.x,self.ciblecharognard.y),Helper.calcDistance(self.x,self.y,self.ciblecharognard.x,self.ciblecharognard.y),self.x,self.y)
                self.x=nouvelleposition[0]
                self.y=nouvelleposition[1]
            else:
                nouvelleposition=Helper.getAngledPoint(Helper.calcAngle(self.x,self.y,self.ciblecharognard.x,self.ciblecharognard.y),self.vitesse,self.x,self.y)
                self.x=nouvelleposition[0]
                self.y=nouvelleposition[1]
                self.getDirection(self.ciblecharognard.x, self.ciblecharognard.y)
                
            #-------------- Tuer la Cible --------------
            if(self.x==self.ciblecharognard.x and self.y==self.ciblecharognard.y):
                self.repas=self.ciblecharognard
                
                for i in parent.loups:
                    if(i.ciblecharognard==self.repas):
                        i.ciblecharoganrd=None
                self.fatigue=0
                        
        else:
            
            #-------------- Acquérir une Cible --------------
            cibletemp=[None,0]
            for i in parent.carcasses:
                if i.type=='R':
                    dist=Helper.calcDistance(self.x,self.y,i.x,i.y)
                    if dist<150:
                        if cibletemp[0]:
                            if(dist<cibletemp[1]):
                                cibletemp=[i,dist]
                        else:
                            cibletemp=[i,dist]
            self.ciblecharognard=cibletemp[0]