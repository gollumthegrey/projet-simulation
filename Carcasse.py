
#Copyright © Gäetan
class Carcasse():
    def __init__(self,x,y,energie,id,type):
        self.x=x
        self.y=y
        self.id=id
        self.energie=energie
        self.age=0
        self.type=type
        self.sexe=None
    
    def evolue(self,parent):
        #DEGRADATION
        self.age+=1
        if(self.age%200==0):
            self.energie-=1
            
        if(self.energie < 0):
            parent.carcasses.remove(self)
            for i in parent.lapins:
                    if(i.repas==self):
                        i.repas=None
            for i in parent.loups:
                    if(i.repas==self):
                        i.repas=None
                    if(i.ciblecharognard==self):
                        i.ciblecharognard=None
    
    
    def manger(self,perte):
        self.energie-=perte
		
		