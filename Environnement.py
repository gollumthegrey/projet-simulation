# -*- coding: utf-8 -*-
import random
import Loup, Lapin, Laitue
import Monde, Controleur

class Environnement():
    def __init__(self,largeur,hauteur,loup,lapin,laitue,coordRiviere,coordForet,coordMontagne):
        self.largeur=largeur
        self.hauteur=hauteur
        self.id=0
        self.coordRiviere=coordRiviere
        self.loups=[]
        self.lapins=[]
        self.laitues=[]
        self.carcasses=[]
        self.peupler(loup,lapin,laitue)
        self.coordForet=coordForet
        self.coordMontagne=coordMontagne
        
        
    #=============PEUPLER AVEC ATTRIBUTS RANDOM=============#
    def peupler(self,loup,lapin,laitue):
        
        #------------LOUPS----------------------------#
        for i in range(loup):
            x=random.randrange(self.largeur)
            y=random.randrange(self.hauteur)
            energie=random.randrange(50,100)
            poids=random.randrange(5,20)
            sexe=random.choice(["M", "F"])            
            self.loups.append(Loup.Loup(x,y,energie,poids,sexe,self.id))
            self.id += 1
            
        #------------LAPINS----------------------------#  
        for i in range(lapin):
            x=random.randrange(self.largeur)
            y=random.randrange(self.hauteur)
            energie=random.randrange(50,100)
            poids=random.randrange(40,200)
            sexe=random.choice(["M", "F"])            
            self.lapins.append(Lapin.Lapin(x,y,energie,poids,sexe,self.id))
            self.id += 1
        #------------LAITUES----------------------------# 
        for i in range(laitue):
            x=random.randrange(self.largeur)
            y=random.randrange(self.hauteur)
            energie=random.randrange(25,40)
            self.laitues.append(Laitue.Laitue(x,y,energie, self.id))
            self.id += 1
    
	#============= ÉVOLUTION DES OBJETS =============#	
    def evolue(self):
        for i in self.lapins:
            i.evolue(self, self.laitues,self.lapins)
        for i in self.loups:
            i.evolue(self, self.loups, self.lapins)
        for i in self.carcasses:
            i.evolue(self)
        for i in self.laitues:
            i.evolue(self)
    
	
    #============= REPRODUCTION DES LAPINS =============#
    def reproductionLapins(self,x,y):
        for i in range(random.randrange(3,5)):
            energie=random.randrange(50,100)
            poids=random.randrange(40,200)
            sexe=random.choice(["M", "F"]) 
            self.lapins.append(Lapin.Lapin(x,y,energie,poids,sexe,self.id))
        
        
    
    #============= REGÉNERER DES LAITUES =============#
    def reproductionLaitue(self,originex,originey):
        for i in range(random.randrange(2,3)):
            x=originex+random.randrange(-10,10)
            y=originey+random.randrange(-10,10)
            energie=random.randrange(25,40)
            self.laitues.append(Laitue.Laitue(x,y,energie,self.id))
            self.id += 1
                
    #============= REPRODUCTION DES LOUPS =============#
         
    def reproductionLoups(self,x,y):
        for i in range(random.randrange(1,2)):
            energie=random.randrange(50,100)
            poids=random.randrange(40,200)
            sexe=random.choice(["M", "F"]) 
            self.loups.append(Loup.Loup(x,y,energie,poids,sexe,self.id))
                   
            